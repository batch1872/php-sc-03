<?php



class Person {
    public $fName;
    public $mName;
    public $lName;

    //constructor
    public function __construct($firstName, $middleName, $lastName){
		$this->f = $firstName;
		$this->m = $middleName;
		$this->l = $lastName;
	}

    //methods
    public function printName(){
		return "Your full name is $this->f $this->l.";
	}
    
}
    class Developer extends Person{
    
        public function printName(){
            return "Your name is $this->f $this->m $this->l and you are a developer. ";
        }
    }
         
    class Engineer extends Person {
    
        public function printName(){
            return "You are an engineer named $this->f, $this->m, $this->l.";
        }
    


    }   

$person = new Person('Senku','Nayura', 'Ishigami');
$developer = new Developer('John','Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reeve');